/*
The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
Then log the car's year, make, and model in the console log in the format of:
*/

const carById = function(inventory){
    const res = inventory.filter((ele) => { //it filters and gives the array of objects whose id == 33. In this case only one object is present in the array.  
        return ele.id == 33;                // [ { id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 } ]
    });

    return `Car 33 is a ${res[0].car_year} ${res[0].car_make} ${res[0].car_model}`;
}

module.exports = carById;