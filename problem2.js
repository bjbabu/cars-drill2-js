/*The dealer needs the information on the last car in their inventory. 
Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:*/

const lastCar = function(inventory) {
    let n = inventory.length;

    return `Last car is a ${inventory[n-1].car_year} ${inventory[n-1].car_make} ${inventory[n-1].car_model}`;

    ///                                             (OR)

    // const res = inventory.filter((ele) => {
    //     return ele.id == n;
    // });

    // return `Last car is a ${res[0].car_year} ${res[0].car_make} ${res[0].car_model}`;
}

module.exports = lastCar;