const inventory = require('./inventory');
const problem4 = require('../problem4');
const problem5 = require('../problem5');

const carYears = problem4(inventory);

console.log(problem5(carYears));